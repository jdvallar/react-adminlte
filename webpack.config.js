const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@vallarj/react-adminlte': path.resolve(__dirname, 'src/components')
        },
    }
};