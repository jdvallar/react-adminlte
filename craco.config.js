const path = require('path');

module.exports = {
    webpack: {
        alias: {
            '@vallarj/react-adminlte': path.resolve(__dirname, 'src/components')
        },
    }
};